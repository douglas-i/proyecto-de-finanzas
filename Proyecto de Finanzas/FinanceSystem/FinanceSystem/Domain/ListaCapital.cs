﻿using System;

namespace FinanceSystem.Domain
{
    public class ListaCapital
    {
        public int idc { get; set; }
        public string cuentac { get; set; }
        public double montoc { get; set; }
    }
}
