﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using DataAccess;

namespace FinanceSystem.DataAcess
{
    class Capital:ConnectionToSql
    {
        public DataTable MostrarCapital()
        {
            using (var coneccion = GetConnection())
            {
                coneccion.Open();

                using (var command = new SqlCommand())
                {
                    command.Connection = coneccion;
                    command.CommandText = "select * from Capital";
                    SqlDataReader leer = command.ExecuteReader();
                    DataTable tabla = new DataTable();
                    tabla.Load(leer);
                    return tabla;
                }
            }
        }

        public void InsertarCapital(string cuenta, double money)
        {
            using (var coneccion = GetConnection())
            {
                coneccion.Open();

                using (var command = new SqlCommand())
                {
                    command.Connection = coneccion;
                    command.CommandText = "INSERT INTO Capital VALUES ('" + cuenta + "'," + money + ")";
                    command.ExecuteNonQuery();
                }
            }
        }
    }
}
