﻿
namespace FinanceSystem
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.panelSideMenu = new System.Windows.Forms.Panel();
            this.panelRazonesSubmenu = new System.Windows.Forms.Panel();
            this.btnRazonesFinancieras = new FontAwesome.Sharp.IconButton();
            this.panelEstadosSubmenu = new System.Windows.Forms.Panel();
            this.btnER = new FontAwesome.Sharp.IconButton();
            this.btnBG = new FontAwesome.Sharp.IconButton();
            this.btnEstadosFinancieros = new FontAwesome.Sharp.IconButton();
            this.panelCatalogoSubmenu = new System.Windows.Forms.Panel();
            this.btnOtros = new FontAwesome.Sharp.IconButton();
            this.btnCapital = new FontAwesome.Sharp.IconButton();
            this.btnPasivos = new FontAwesome.Sharp.IconButton();
            this.btnActivos = new FontAwesome.Sharp.IconButton();
            this.btnCatalogo = new FontAwesome.Sharp.IconButton();
            this.panelLogo = new System.Windows.Forms.Panel();
            this.panelSecundario = new System.Windows.Forms.Panel();
            this.panelChildForm = new System.Windows.Forms.Panel();
            this.btnExit = new FontAwesome.Sharp.IconButton();
            this.btnMinimizar = new FontAwesome.Sharp.IconButton();
            this.panelSideMenu.SuspendLayout();
            this.panelEstadosSubmenu.SuspendLayout();
            this.panelCatalogoSubmenu.SuspendLayout();
            this.panelSecundario.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelSideMenu
            // 
            this.panelSideMenu.AutoScroll = true;
            this.panelSideMenu.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(31)))), ((int)(((byte)(30)))), ((int)(((byte)(68)))));
            this.panelSideMenu.Controls.Add(this.panelRazonesSubmenu);
            this.panelSideMenu.Controls.Add(this.btnRazonesFinancieras);
            this.panelSideMenu.Controls.Add(this.panelEstadosSubmenu);
            this.panelSideMenu.Controls.Add(this.btnEstadosFinancieros);
            this.panelSideMenu.Controls.Add(this.panelCatalogoSubmenu);
            this.panelSideMenu.Controls.Add(this.btnCatalogo);
            this.panelSideMenu.Controls.Add(this.panelLogo);
            this.panelSideMenu.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelSideMenu.Location = new System.Drawing.Point(0, 0);
            this.panelSideMenu.Name = "panelSideMenu";
            this.panelSideMenu.Size = new System.Drawing.Size(250, 600);
            this.panelSideMenu.TabIndex = 0;
            // 
            // panelRazonesSubmenu
            // 
            this.panelRazonesSubmenu.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(32)))), ((int)(((byte)(39)))));
            this.panelRazonesSubmenu.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelRazonesSubmenu.Location = new System.Drawing.Point(0, 471);
            this.panelRazonesSubmenu.Name = "panelRazonesSubmenu";
            this.panelRazonesSubmenu.Size = new System.Drawing.Size(233, 165);
            this.panelRazonesSubmenu.TabIndex = 5;
            // 
            // btnRazonesFinancieras
            // 
            this.btnRazonesFinancieras.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnRazonesFinancieras.FlatAppearance.BorderSize = 0;
            this.btnRazonesFinancieras.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnRazonesFinancieras.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnRazonesFinancieras.IconChar = FontAwesome.Sharp.IconChar.Poll;
            this.btnRazonesFinancieras.IconColor = System.Drawing.Color.White;
            this.btnRazonesFinancieras.IconFont = FontAwesome.Sharp.IconFont.Auto;
            this.btnRazonesFinancieras.IconSize = 32;
            this.btnRazonesFinancieras.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnRazonesFinancieras.Location = new System.Drawing.Point(0, 426);
            this.btnRazonesFinancieras.Name = "btnRazonesFinancieras";
            this.btnRazonesFinancieras.Size = new System.Drawing.Size(233, 45);
            this.btnRazonesFinancieras.TabIndex = 8;
            this.btnRazonesFinancieras.Text = "Razones Financieras";
            this.btnRazonesFinancieras.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnRazonesFinancieras.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnRazonesFinancieras.UseVisualStyleBackColor = true;
            this.btnRazonesFinancieras.Click += new System.EventHandler(this.btnRazonesFinancieras_Click);
            // 
            // panelEstadosSubmenu
            // 
            this.panelEstadosSubmenu.Controls.Add(this.btnER);
            this.panelEstadosSubmenu.Controls.Add(this.btnBG);
            this.panelEstadosSubmenu.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelEstadosSubmenu.Location = new System.Drawing.Point(0, 344);
            this.panelEstadosSubmenu.Name = "panelEstadosSubmenu";
            this.panelEstadosSubmenu.Size = new System.Drawing.Size(233, 82);
            this.panelEstadosSubmenu.TabIndex = 6;
            // 
            // btnER
            // 
            this.btnER.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(249)))), ((int)(((byte)(118)))), ((int)(((byte)(176)))));
            this.btnER.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnER.IconChar = FontAwesome.Sharp.IconChar.None;
            this.btnER.IconColor = System.Drawing.Color.Black;
            this.btnER.IconFont = FontAwesome.Sharp.IconFont.Auto;
            this.btnER.Location = new System.Drawing.Point(0, 40);
            this.btnER.Name = "btnER";
            this.btnER.Size = new System.Drawing.Size(233, 40);
            this.btnER.TabIndex = 8;
            this.btnER.Text = "Estado de Resultado";
            this.btnER.UseVisualStyleBackColor = false;
            this.btnER.Click += new System.EventHandler(this.btnER_Click);
            // 
            // btnBG
            // 
            this.btnBG.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(249)))), ((int)(((byte)(118)))), ((int)(((byte)(176)))));
            this.btnBG.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnBG.IconChar = FontAwesome.Sharp.IconChar.None;
            this.btnBG.IconColor = System.Drawing.Color.Black;
            this.btnBG.IconFont = FontAwesome.Sharp.IconFont.Auto;
            this.btnBG.Location = new System.Drawing.Point(0, 0);
            this.btnBG.Name = "btnBG";
            this.btnBG.Size = new System.Drawing.Size(233, 40);
            this.btnBG.TabIndex = 7;
            this.btnBG.Text = "Balance General";
            this.btnBG.UseVisualStyleBackColor = false;
            this.btnBG.Click += new System.EventHandler(this.btnBG_Click);
            // 
            // btnEstadosFinancieros
            // 
            this.btnEstadosFinancieros.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnEstadosFinancieros.FlatAppearance.BorderSize = 0;
            this.btnEstadosFinancieros.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnEstadosFinancieros.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnEstadosFinancieros.IconChar = FontAwesome.Sharp.IconChar.Paste;
            this.btnEstadosFinancieros.IconColor = System.Drawing.Color.White;
            this.btnEstadosFinancieros.IconFont = FontAwesome.Sharp.IconFont.Auto;
            this.btnEstadosFinancieros.IconSize = 32;
            this.btnEstadosFinancieros.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnEstadosFinancieros.Location = new System.Drawing.Point(0, 299);
            this.btnEstadosFinancieros.Name = "btnEstadosFinancieros";
            this.btnEstadosFinancieros.Size = new System.Drawing.Size(233, 45);
            this.btnEstadosFinancieros.TabIndex = 7;
            this.btnEstadosFinancieros.Text = "Estados Financieros";
            this.btnEstadosFinancieros.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnEstadosFinancieros.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnEstadosFinancieros.UseVisualStyleBackColor = true;
            this.btnEstadosFinancieros.Click += new System.EventHandler(this.btnEstadosFinancieros_Click);
            // 
            // panelCatalogoSubmenu
            // 
            this.panelCatalogoSubmenu.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(31)))), ((int)(((byte)(30)))), ((int)(((byte)(68)))));
            this.panelCatalogoSubmenu.Controls.Add(this.btnOtros);
            this.panelCatalogoSubmenu.Controls.Add(this.btnCapital);
            this.panelCatalogoSubmenu.Controls.Add(this.btnPasivos);
            this.panelCatalogoSubmenu.Controls.Add(this.btnActivos);
            this.panelCatalogoSubmenu.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelCatalogoSubmenu.Location = new System.Drawing.Point(0, 134);
            this.panelCatalogoSubmenu.Name = "panelCatalogoSubmenu";
            this.panelCatalogoSubmenu.Size = new System.Drawing.Size(233, 165);
            this.panelCatalogoSubmenu.TabIndex = 1;
            // 
            // btnOtros
            // 
            this.btnOtros.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(24)))), ((int)(((byte)(161)))), ((int)(((byte)(251)))));
            this.btnOtros.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnOtros.IconChar = FontAwesome.Sharp.IconChar.None;
            this.btnOtros.IconColor = System.Drawing.Color.Black;
            this.btnOtros.IconFont = FontAwesome.Sharp.IconFont.Auto;
            this.btnOtros.Location = new System.Drawing.Point(0, 120);
            this.btnOtros.Name = "btnOtros";
            this.btnOtros.Size = new System.Drawing.Size(233, 40);
            this.btnOtros.TabIndex = 9;
            this.btnOtros.Text = "Otras Cuentas";
            this.btnOtros.UseVisualStyleBackColor = false;
            this.btnOtros.Click += new System.EventHandler(this.btnOtros_Click);
            // 
            // btnCapital
            // 
            this.btnCapital.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(24)))), ((int)(((byte)(161)))), ((int)(((byte)(251)))));
            this.btnCapital.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnCapital.IconChar = FontAwesome.Sharp.IconChar.None;
            this.btnCapital.IconColor = System.Drawing.Color.Black;
            this.btnCapital.IconFont = FontAwesome.Sharp.IconFont.Auto;
            this.btnCapital.Location = new System.Drawing.Point(0, 80);
            this.btnCapital.Name = "btnCapital";
            this.btnCapital.Size = new System.Drawing.Size(233, 40);
            this.btnCapital.TabIndex = 8;
            this.btnCapital.Text = "Capital";
            this.btnCapital.UseVisualStyleBackColor = false;
            this.btnCapital.Click += new System.EventHandler(this.btnCapital_Click);
            // 
            // btnPasivos
            // 
            this.btnPasivos.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(24)))), ((int)(((byte)(161)))), ((int)(((byte)(251)))));
            this.btnPasivos.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnPasivos.IconChar = FontAwesome.Sharp.IconChar.None;
            this.btnPasivos.IconColor = System.Drawing.Color.Black;
            this.btnPasivos.IconFont = FontAwesome.Sharp.IconFont.Auto;
            this.btnPasivos.Location = new System.Drawing.Point(0, 40);
            this.btnPasivos.Name = "btnPasivos";
            this.btnPasivos.Size = new System.Drawing.Size(233, 40);
            this.btnPasivos.TabIndex = 7;
            this.btnPasivos.Text = "Pasivos";
            this.btnPasivos.UseVisualStyleBackColor = false;
            this.btnPasivos.Click += new System.EventHandler(this.btnPasivos_Click);
            // 
            // btnActivos
            // 
            this.btnActivos.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(24)))), ((int)(((byte)(161)))), ((int)(((byte)(251)))));
            this.btnActivos.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnActivos.IconChar = FontAwesome.Sharp.IconChar.None;
            this.btnActivos.IconColor = System.Drawing.Color.Black;
            this.btnActivos.IconFont = FontAwesome.Sharp.IconFont.Auto;
            this.btnActivos.Location = new System.Drawing.Point(0, 0);
            this.btnActivos.Name = "btnActivos";
            this.btnActivos.Size = new System.Drawing.Size(233, 40);
            this.btnActivos.TabIndex = 6;
            this.btnActivos.Text = "Activos";
            this.btnActivos.UseVisualStyleBackColor = false;
            this.btnActivos.Click += new System.EventHandler(this.btnActivos_Click);
            // 
            // btnCatalogo
            // 
            this.btnCatalogo.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnCatalogo.FlatAppearance.BorderSize = 0;
            this.btnCatalogo.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCatalogo.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnCatalogo.IconChar = FontAwesome.Sharp.IconChar.AlignLeft;
            this.btnCatalogo.IconColor = System.Drawing.Color.White;
            this.btnCatalogo.IconFont = FontAwesome.Sharp.IconFont.Auto;
            this.btnCatalogo.IconSize = 32;
            this.btnCatalogo.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnCatalogo.Location = new System.Drawing.Point(0, 89);
            this.btnCatalogo.Name = "btnCatalogo";
            this.btnCatalogo.Size = new System.Drawing.Size(233, 45);
            this.btnCatalogo.TabIndex = 6;
            this.btnCatalogo.Text = "Catalogo";
            this.btnCatalogo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnCatalogo.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnCatalogo.UseVisualStyleBackColor = true;
            this.btnCatalogo.Click += new System.EventHandler(this.btnCatalogo_Click);
            // 
            // panelLogo
            // 
            this.panelLogo.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelLogo.Location = new System.Drawing.Point(0, 0);
            this.panelLogo.Name = "panelLogo";
            this.panelLogo.Size = new System.Drawing.Size(233, 89);
            this.panelLogo.TabIndex = 1;
            // 
            // panelSecundario
            // 
            this.panelSecundario.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(26)))), ((int)(((byte)(25)))), ((int)(((byte)(62)))));
            this.panelSecundario.Controls.Add(this.btnMinimizar);
            this.panelSecundario.Controls.Add(this.btnExit);
            this.panelSecundario.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelSecundario.Location = new System.Drawing.Point(250, 0);
            this.panelSecundario.Name = "panelSecundario";
            this.panelSecundario.Size = new System.Drawing.Size(784, 59);
            this.panelSecundario.TabIndex = 1;
            this.panelSecundario.MouseDown += new System.Windows.Forms.MouseEventHandler(this.panelSecundario_MouseDown);
            // 
            // panelChildForm
            // 
            this.panelChildForm.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(15)))), ((int)(((byte)(11)))), ((int)(((byte)(21)))));
            this.panelChildForm.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelChildForm.Location = new System.Drawing.Point(250, 59);
            this.panelChildForm.Name = "panelChildForm";
            this.panelChildForm.Size = new System.Drawing.Size(784, 541);
            this.panelChildForm.TabIndex = 2;
            // 
            // btnExit
            // 
            this.btnExit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnExit.FlatAppearance.BorderSize = 0;
            this.btnExit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnExit.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnExit.IconChar = FontAwesome.Sharp.IconChar.WindowClose;
            this.btnExit.IconColor = System.Drawing.Color.White;
            this.btnExit.IconFont = FontAwesome.Sharp.IconFont.Auto;
            this.btnExit.IconSize = 18;
            this.btnExit.Location = new System.Drawing.Point(752, 5);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(27, 21);
            this.btnExit.TabIndex = 8;
            this.btnExit.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnExit.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnExit.UseVisualStyleBackColor = true;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // btnMinimizar
            // 
            this.btnMinimizar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnMinimizar.FlatAppearance.BorderSize = 0;
            this.btnMinimizar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMinimizar.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnMinimizar.IconChar = FontAwesome.Sharp.IconChar.WindowMinimize;
            this.btnMinimizar.IconColor = System.Drawing.Color.White;
            this.btnMinimizar.IconFont = FontAwesome.Sharp.IconFont.Auto;
            this.btnMinimizar.IconSize = 18;
            this.btnMinimizar.Location = new System.Drawing.Point(730, 3);
            this.btnMinimizar.Name = "btnMinimizar";
            this.btnMinimizar.Size = new System.Drawing.Size(27, 21);
            this.btnMinimizar.TabIndex = 9;
            this.btnMinimizar.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnMinimizar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnMinimizar.UseVisualStyleBackColor = true;
            this.btnMinimizar.Click += new System.EventHandler(this.btnMinimizar_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(1034, 600);
            this.Controls.Add(this.panelChildForm);
            this.Controls.Add(this.panelSecundario);
            this.Controls.Add(this.panelSideMenu);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ForeColor = System.Drawing.SystemColors.ControlText;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MinimumSize = new System.Drawing.Size(950, 600);
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Form1";
            this.panelSideMenu.ResumeLayout(false);
            this.panelEstadosSubmenu.ResumeLayout(false);
            this.panelCatalogoSubmenu.ResumeLayout(false);
            this.panelSecundario.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panelSideMenu;
        private System.Windows.Forms.Panel panelCatalogoSubmenu;
        private System.Windows.Forms.Panel panelLogo;
        private System.Windows.Forms.Panel panelRazonesSubmenu;
        private System.Windows.Forms.Panel panelSecundario;
        private System.Windows.Forms.Panel panelChildForm;
        private FontAwesome.Sharp.IconButton btnCatalogo;
        private FontAwesome.Sharp.IconButton btnActivos;
        private FontAwesome.Sharp.IconButton btnOtros;
        private FontAwesome.Sharp.IconButton btnCapital;
        private FontAwesome.Sharp.IconButton btnPasivos;
        private FontAwesome.Sharp.IconButton btnEstadosFinancieros;
        private System.Windows.Forms.Panel panelEstadosSubmenu;
        private FontAwesome.Sharp.IconButton btnER;
        private FontAwesome.Sharp.IconButton btnBG;
        private FontAwesome.Sharp.IconButton btnRazonesFinancieras;
        private FontAwesome.Sharp.IconButton btnMinimizar;
        private FontAwesome.Sharp.IconButton btnExit;
    }
}

