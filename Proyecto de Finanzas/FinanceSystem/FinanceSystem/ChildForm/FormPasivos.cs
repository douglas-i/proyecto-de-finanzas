﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using FinanceSystem.Domain;

namespace FinanceSystem.ChildForm
{
    public partial class FormPasivos : Form
    {
        _Pasivo pasivo = new _Pasivo();
        private string idPas = null;
        private bool editar = false;

        public FormPasivos()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void FormPasivos_Load(object sender, EventArgs e)
        {
            MostrarPasivos();
        }

        private void MostrarPasivos()//Metodo para mostrar los pasivos que se encuentra en la base de datos
        {
            dataGridView1.DataSource = pasivo.MostrarPasivo();
            dataGridView1.Columns[0].Width = 65;
            dataGridView1.Columns[1].Width = 364;
            dataGridView1.Columns[2].Width = 192;
        }

        private void btnIngresar_Click(object sender, EventArgs e)
        {
            try
            {
                pasivo.InsertarPas(txtCuenta.Texts, txtMonto.Texts);
                MessageBox.Show("Se Inserto Correctamente");
                MostrarPasivos();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error al insertar los datos por: " + ex);
            }
        }
    }
}
