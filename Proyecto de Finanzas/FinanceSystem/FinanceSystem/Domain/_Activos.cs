﻿using FinanceSystem.DataAcess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;

namespace FinanceSystem.Domain
{
    public class _Activos:Activos
    {
        public DataTable MostrarAct()
        {
            DataTable tabla = new DataTable();
            tabla = MostrarActivo();
            return tabla;
        }

        public void InsertarAct(string cuenta, string money)
        {
            InsertarActivo(cuenta, Convert.ToDouble(money));
        }

        public void EditrAct(string cuenta, string money, string id)
        {
            EditarActivo(cuenta, Convert.ToDouble(money), Convert.ToInt32(id));
        }
    }
}
