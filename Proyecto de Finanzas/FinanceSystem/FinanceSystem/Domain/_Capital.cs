﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using FinanceSystem.DataAcess;

namespace FinanceSystem.Domain
{
    class _Capital:Capital
    {
        public DataTable MostraCap()
        {
            DataTable tabla = new DataTable();
            tabla = MostraCap();
            return tabla;
        }

        public void InsertarCap(string cuenta, string money)
        {
            InsertarCapital(cuenta, Convert.ToDouble(money));
        }
    }
}
