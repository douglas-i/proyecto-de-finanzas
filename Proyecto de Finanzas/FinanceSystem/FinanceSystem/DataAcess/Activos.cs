﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;
using DataAccess;

namespace FinanceSystem.DataAcess
{
    public class Activos:ConnectionToSql
    {                      

        public DataTable MostrarActivo()
        {
            using (var coneccion = GetConnection())
            {
                coneccion.Open();

                using (var command = new SqlCommand())
                {
                    command.Connection = coneccion;
                    command.CommandText = "select * from Activo";
                    SqlDataReader leer = command.ExecuteReader();
                    DataTable tabla = new DataTable();
                    tabla.Load(leer);
                    return tabla;
                }
            }
        }

        public void InsertarActivo(string cuenta, double money)
        {
            using (var coneccion = GetConnection())
            {
                coneccion.Open();

                using (var command = new SqlCommand())
                {
                    command.Connection = coneccion;
                    command.CommandText = "INSERT INTO Activo VALUES ('" + cuenta + "'," + money + ")";
                    command.ExecuteNonQuery();                    
                }
            }
        }

        public void EditarActivo(string cuenta, double money, int id)
        {
            using (var coneccion = GetConnection())
            {
                coneccion.Open();

                using (var command = new SqlCommand())
                {
                    command.Connection = coneccion;
                    command.CommandText = "Editar";
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@cuenta", cuenta);
                    command.Parameters.AddWithValue("@money", money);
                    command.Parameters.AddWithValue("@id", id);
                    command.ExecuteNonQuery();
                }
            }
        }        
    }
}
