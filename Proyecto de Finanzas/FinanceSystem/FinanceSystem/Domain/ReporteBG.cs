﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FinanceSystem.DataAcess;
using System.Data;
namespace FinanceSystem.Domain
{
    public class ReporteBG:Activos
    {
        public DateTime fechaReporte { get; set; }
        public List <ListaActivos>  listaActivos { get; private set; }
        public List<ListaPasivos> listaPasivos { get; private set; }
        public List<ListaCapital> listaCapital { get; private set; }
        public double totalActivos { get; private set; }
        public double totalPasivos { get; private set; }
        public double totalCapital { get; private set; }
        public double pas_cap { get; private set; }

        //Metodo para realizar el reporte
        public void createReporteBG()
        {
            //Fecha
            fechaReporte = DateTime.Now;

            //Lista Activos
            var activos = new Activos();
            var resActivo = activos.MostrarActivo();

            listaActivos = new List<ListaActivos>();

            foreach(System.Data.DataRow rows in resActivo.Rows)
            {
                var listaActivosmodel = new ListaActivos()
                {
                    id = Convert.ToInt32(rows[0]),
                    cuenta = Convert.ToString(rows[1]),
                    monto = Convert.ToDouble(rows[2])
                };

                listaActivos.Add(listaActivosmodel);

                //Calculo total de Activos
                totalActivos += Convert.ToDouble(rows[2]);                
            }

            totalActivos = totalActivos - 509.7;

            //Lista Pasivos
            var pasivos = new Pasivos();
            var resPasivo = pasivos.MostrarPasivo();

            listaPasivos = new List<ListaPasivos>();

            foreach(System.Data.DataRow rows in resPasivo.Rows)
            {
                var listaPasivosmodel = new ListaPasivos()
                {
                    idp = Convert.ToInt32(rows[0]),
                    cuentap = Convert.ToString(rows[1]),
                    montop = Convert.ToDouble(rows[2])
                };

                listaPasivos.Add(listaPasivosmodel);

                //Calculo total de Pasivos
                totalPasivos += Convert.ToDouble(rows[2]);
            }

            //Lista Capital
            var Capital = new Capital();
            var resCapital = Capital.MostrarCapital();

            listaCapital = new List<ListaCapital>();

            foreach(System.Data.DataRow rows in resCapital.Rows)
            {
                var listaCapitalmodel = new ListaCapital()
                {
                    idc = Convert.ToInt32(rows[0]),
                    cuentac = Convert.ToString(rows[1]),
                    montoc = Convert.ToDouble(rows[2])
                };

                listaCapital.Add(listaCapitalmodel);

                //Calculo toal de Capital
                totalCapital += Convert.ToDouble(rows[2]);
            }

            //Total capital + pasivo
            pas_cap = totalPasivos + totalCapital;
        }
    }
}
