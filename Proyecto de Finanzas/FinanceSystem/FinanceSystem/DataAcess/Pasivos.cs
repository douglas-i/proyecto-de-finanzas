﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;
using DataAccess;

namespace FinanceSystem.DataAcess
{
    class Pasivos:ConnectionToSql
    {
        public DataTable MostrarPasivo()
        {
            using (var coneccion = GetConnection())
            {
                coneccion.Open();

                using (var command = new SqlCommand())
                {
                    command.Connection = coneccion;
                    command.CommandText = "select * from Pasivo";
                    SqlDataReader leer = command.ExecuteReader();
                    DataTable tabla = new DataTable();
                    tabla.Load(leer);
                    return tabla;
                }
            }
        }

        public void InsertarPasivo(string cuenta, double money)
        {
            using (var coneccion = GetConnection())
            {
                coneccion.Open();

                using (var command = new SqlCommand())
                {
                    command.Connection = coneccion;
                    command.CommandText = "INSERT INTO Pasivo VALUES ('" + cuenta + "'," + money + ")";
                    command.ExecuteNonQuery();
                }
            }
        }
    }
}
