CREATE DATABASE FinanceSystem
GO

USE FinanceSystem
GO

CREATE TABLE Activo
([ID] INT IDENTITY (1,1) PRIMARY KEY,
 [CUENTA] NVARCHAR(100) NOT NULL,
 [MONTO] MONEY NOT NULL)
 GO

 CREATE TABLE Pasivo
([ID] INT IDENTITY (1,1) PRIMARY KEY,
 [CUENTA] NVARCHAR(100) NOT NULL,
 [MONTO] MONEY NOT NULL)
 GO

 CREATE TABLE Capital
([ID] INT IDENTITY (1,1) PRIMARY KEY,
 [CUENTA] NVARCHAR(100) NOT NULL,
 [MONTO] MONEY NOT NULL)
 GO

 INSERT INTO Activo VALUES 
 ('Caja',20221.20),
 ('Banco',11638.80),
 ('Inventario',24000.00)
GO

select * from Capital
GO

 CREATE PROCEDURE Editar 
 @cuenta NVARCHAR(100),
 @money MONEY,
 @id int
 AS
 UPDATE Activo SET CUENTA = @cuenta, MONTO = @money WHERE ID = @id
GO

EXEC Editar 'Caja',1000,1