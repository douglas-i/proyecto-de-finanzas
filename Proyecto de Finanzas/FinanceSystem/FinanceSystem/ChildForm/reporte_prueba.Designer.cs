﻿
namespace FinanceSystem.ChildForm
{
    partial class reporte_prueba
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Microsoft.Reporting.WinForms.ReportDataSource reportDataSource1 = new Microsoft.Reporting.WinForms.ReportDataSource();
            Microsoft.Reporting.WinForms.ReportDataSource reportDataSource2 = new Microsoft.Reporting.WinForms.ReportDataSource();
            Microsoft.Reporting.WinForms.ReportDataSource reportDataSource3 = new Microsoft.Reporting.WinForms.ReportDataSource();
            Microsoft.Reporting.WinForms.ReportDataSource reportDataSource4 = new Microsoft.Reporting.WinForms.ReportDataSource();
            this.reportViewer1 = new Microsoft.Reporting.WinForms.ReportViewer();
            this.ReporteBGBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.ListaActivosBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.ListaPasivosBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.ListaCapitalBindingSource = new System.Windows.Forms.BindingSource(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.ReporteBGBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ListaActivosBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ListaPasivosBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ListaCapitalBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // reportViewer1
            // 
            this.reportViewer1.Dock = System.Windows.Forms.DockStyle.Fill;
            reportDataSource1.Name = "ReporteBG";
            reportDataSource1.Value = this.ReporteBGBindingSource;
            reportDataSource2.Name = "ListaActivos";
            reportDataSource2.Value = this.ListaActivosBindingSource;
            reportDataSource3.Name = "ListaPasivos";
            reportDataSource3.Value = this.ListaPasivosBindingSource;
            reportDataSource4.Name = "ListaCapital";
            reportDataSource4.Value = this.ListaCapitalBindingSource;
            this.reportViewer1.LocalReport.DataSources.Add(reportDataSource1);
            this.reportViewer1.LocalReport.DataSources.Add(reportDataSource2);
            this.reportViewer1.LocalReport.DataSources.Add(reportDataSource3);
            this.reportViewer1.LocalReport.DataSources.Add(reportDataSource4);
            this.reportViewer1.LocalReport.ReportEmbeddedResource = "FinanceSystem.Reports.ReporteBG.rdlc";
            this.reportViewer1.Location = new System.Drawing.Point(0, 0);
            this.reportViewer1.Name = "reportViewer1";
            this.reportViewer1.ServerReport.BearerToken = null;
            this.reportViewer1.Size = new System.Drawing.Size(538, 428);
            this.reportViewer1.TabIndex = 0;
            // 
            // ReporteBGBindingSource
            // 
            this.ReporteBGBindingSource.DataSource = typeof(FinanceSystem.Domain.ReporteBG);
            // 
            // ListaActivosBindingSource
            // 
            this.ListaActivosBindingSource.DataSource = typeof(FinanceSystem.Domain.ListaActivos);
            // 
            // ListaPasivosBindingSource
            // 
            this.ListaPasivosBindingSource.DataSource = typeof(FinanceSystem.Domain.ListaPasivos);
            // 
            // ListaCapitalBindingSource
            // 
            this.ListaCapitalBindingSource.DataSource = typeof(FinanceSystem.Domain.ListaCapital);
            // 
            // reporte_prueba
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.WindowText;
            this.ClientSize = new System.Drawing.Size(538, 428);
            this.Controls.Add(this.reportViewer1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "reporte_prueba";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "reporte_prueba";
            this.Load += new System.EventHandler(this.reporte_prueba_Load);
            ((System.ComponentModel.ISupportInitialize)(this.ReporteBGBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ListaActivosBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ListaPasivosBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ListaCapitalBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Microsoft.Reporting.WinForms.ReportViewer reportViewer1;
        private System.Windows.Forms.BindingSource ReporteBGBindingSource;
        private System.Windows.Forms.BindingSource ListaActivosBindingSource;
        private System.Windows.Forms.BindingSource ListaPasivosBindingSource;
        private System.Windows.Forms.BindingSource ListaCapitalBindingSource;
    }
}