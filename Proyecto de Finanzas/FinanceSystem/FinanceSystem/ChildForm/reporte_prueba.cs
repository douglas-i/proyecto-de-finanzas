﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using FinanceSystem.Domain;

namespace FinanceSystem.ChildForm
{
    public partial class reporte_prueba : Form
    {
        public reporte_prueba()
        {
            InitializeComponent();

            //------------------------------------------------------------
            ReporteBG reporteBGModel = new ReporteBG();
            reporteBGModel.createReporteBG();

            ReporteBGBindingSource.DataSource = reporteBGModel;
            ListaActivosBindingSource.DataSource = reporteBGModel.listaActivos;
            ListaPasivosBindingSource.DataSource = reporteBGModel.listaPasivos;
            ListaCapitalBindingSource.DataSource = reporteBGModel.listaCapital;

            this.reportViewer1.RefreshReport();
        }

        private void reporte_prueba_Load(object sender, EventArgs e)
        {

            this.reportViewer1.RefreshReport();
            
        }
    }
}
