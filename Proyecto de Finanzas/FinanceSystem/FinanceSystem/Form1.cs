﻿using FinanceSystem.ChildForm;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using FontAwesome.Sharp;
using System.Runtime.InteropServices;

namespace FinanceSystem
{
    public partial class Form1 : Form
    {
        //Fields
        private IconButton currentBtn;
        private Panel leftBordelBtn;
        private Form currentChildForm;

        public Form1()
        {
            InitializeComponent();
            customizeDesing();
            this.Text = string.Empty;
            this.ControlBox = false;
            this.DoubleBuffered = true;
            this.MaximizedBounds = Screen.FromHandle(this.Handle).WorkingArea;
        }

        #region Metodos de los SubMenu
        private void customizeDesing()
        {
            panelCatalogoSubmenu.Visible = false;
            panelEstadosSubmenu.Visible = false;
            panelRazonesSubmenu.Visible = false;
        }

        private void hideSubMenu()
        {
            if (panelCatalogoSubmenu.Visible == true)
                panelCatalogoSubmenu.Visible = false;
            if (panelEstadosSubmenu.Visible == true)
                panelEstadosSubmenu.Visible = false;
            if (panelRazonesSubmenu.Visible == true)
                panelRazonesSubmenu.Visible = false;
        }

        private void showSubMenu(Panel subMenu)
        {
            if (subMenu.Visible == false)
            {
                hideSubMenu();
                subMenu.Visible = true;
            }
            else
                subMenu.Visible = false;
        }
        #endregion

        #region Catalogo
        private void btnCatalogo_Click(object sender, EventArgs e)
        {
            ActivateButton(sender, RGBColors.color6);
            showSubMenu(panelCatalogoSubmenu);
        }

        private void btnActivos_Click(object sender, EventArgs e)
        {
            openChildForm(new FormActivos());
            //..
            //Code
            //..
            hideSubMenu();
        }

        private void btnPasivos_Click(object sender, EventArgs e)
        {
            openChildForm(new FormPasivos());
            //..
            //Code
            //..
            hideSubMenu();
        }

        private void btnCapital_Click(object sender, EventArgs e)
        {
            openChildForm(new FormCapital());
            //..
            //Code
            //..
            hideSubMenu();
        }

        private void btnOtros_Click(object sender, EventArgs e)
        {
            //..
            //Code
            //..
            hideSubMenu();
        }
        #endregion

        #region Estados_Financieros
        private void btnEstadosFinancieros_Click(object sender, EventArgs e)
        {
            showSubMenu(panelEstadosSubmenu);
            ActivateButton(sender, RGBColors.color2);
        }

        private void btnBG_Click(object sender, EventArgs e)
        {
            openChildForm(new reporte_prueba());

            //..
            //Code
            //..
            hideSubMenu();
        }

        private void btnER_Click(object sender, EventArgs e)
        {
            //..
            //Code
            //..
            hideSubMenu();
        }
        #endregion

        #region Razones Financieras
        private void btnRazonesFinancieras_Click(object sender, EventArgs e)
        {
            ActivateButton(sender, RGBColors.color1);
        }
        #endregion

        #region Metodo para formulario hijos
        //Metodo para abrir formularios hijos
        private Form activeForm = null;
        private void openChildForm(Form childForm)
        {
            if (activeForm != null)
                activeForm.Close();
            activeForm = childForm;
            childForm.TopLevel = false;
            childForm.FormBorderStyle = FormBorderStyle.None;
            childForm.Dock = DockStyle.Fill;
            panelChildForm.Controls.Add(childForm);
            panelChildForm.Tag = childForm;
            childForm.Show();
        }
        #endregion

        #region Estructura
        //Estructura
        private struct RGBColors
        {
            public static Color color1 = Color.FromArgb(0, 143, 57);
            public static Color color2 = Color.FromArgb(249, 118, 176);
            public static Color color3 = Color.FromArgb(253, 138, 114);
            public static Color color4 = Color.FromArgb(95, 77, 221);
            public static Color color5 = Color.FromArgb(249, 88, 155);
            public static Color color6 = Color.FromArgb(24, 161, 251);
        }
        #endregion

        #region Metodos para los botenes
        //Metodos para los botones
        private void ActivateButton(object senderBtn, Color color) //Metodo para activar el boton
        {
            if (senderBtn != null)
            {
                DisableButton();
                //Button
                currentBtn = (IconButton)senderBtn;
                currentBtn.BackColor = Color.FromArgb(37, 36, 81);
                currentBtn.ForeColor = color;
                currentBtn.TextAlign = ContentAlignment.MiddleCenter;
                currentBtn.IconColor = color;
                currentBtn.TextImageRelation = TextImageRelation.TextBeforeImage;
                currentBtn.ImageAlign = ContentAlignment.MiddleRight;

                #region codigo por revisar
                //left border button
                //leftBordelBtn.BackColor = color;
                //leftBordelBtn.Location = new Point(0, currentBtn.Location.Y);
                //leftBordelBtn.Visible = true;
                //leftBordelBtn.BringToFront();

                //Icon Current Child Form 
                //iconCurrentChildForm.IconChar = currentBtn.IconChar;
                //iconCurrentChildForm.IconColor = color;
                #endregion
            }
        }

        private void DisableButton() //Desactivar el resaltado del Boton
        {
            if (currentBtn != null)
            {
                currentBtn.BackColor = Color.FromArgb(31, 30, 68);
                currentBtn.ForeColor = Color.Gainsboro;
                currentBtn.TextAlign = ContentAlignment.MiddleLeft;
                currentBtn.IconColor = Color.Gainsboro;
                currentBtn.TextImageRelation = TextImageRelation.ImageBeforeText;
                currentBtn.ImageAlign = ContentAlignment.MiddleLeft;
            }
        }
        #endregion

        #region Drag Form
        [DllImport("user32.DLL", EntryPoint = "ReleaseCapture")]
        private extern static void ReleaseCapture();
        [DllImport("user32.DLL", EntryPoint = "SendMessage")]
        private extern static void SendMessage(System.IntPtr hWnd, int wMsg, int wParam, int lParam);

        private void panelSecundario_MouseDown(object sender, MouseEventArgs e)
        {
            ReleaseCapture();
            SendMessage(this.Handle, 0x112, 0xf012, 0);
        }
        #endregion

        #region Botones Exit, Minimizar
        private void btnExit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        //private void btnMaximizar_Click(object sender, EventArgs e)
        //{
        //    if (WindowState == FormWindowState.Normal)
        //        WindowState = FormWindowState.Maximized;
        //    else
        //        WindowState = FormWindowState.Normal;
        //}

        private void btnMinimizar_Click(object sender, EventArgs e)
        {
            WindowState = FormWindowState.Minimized;
        }

        #endregion
        
    }
}
