﻿using System;

namespace FinanceSystem.Domain
{
    public class ListaPasivos
    {
        public int idp { get; set; }
        public string cuentap { get; set; }
        public double montop { get; set; }
    }
}