﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using FinanceSystem.Domain;

namespace FinanceSystem.ChildForm
{
    public partial class FormActivos : Form
    {
        _Activos activos = new _Activos();
        private string idAct = null;
        private bool editar = false;

        public FormActivos()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void FormActivos_Load(object sender, EventArgs e)
        {
            MostrarActivos();
        }

        private void MostrarActivos()//Metodo para mostrar los activos que se encuentra en la base de datos
        {
            dataGridView1.DataSource = activos.MostrarActivo();
            dataGridView1.Columns[0].Width = 65;
            dataGridView1.Columns[1].Width = 364;
            dataGridView1.Columns[2].Width = 192;
        }

        private void btnIngresar_Click(object sender, EventArgs e)
        {
            if (editar == false)
            {
                try
                {
                    activos.InsertarAct(txtCuenta.Texts, txtMonto.Texts);
                    MessageBox.Show("Se Inserto Correctamente");
                    MostrarActivos();
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Error al insertar los datos por: " + ex);
                }
            }
            if (editar == true)
            {
                try
                {
                    activos.EditrAct(txtCuenta.Texts, txtMonto.Texts, idAct);
                    MessageBox.Show("Se Edito Correctamente");
                    MostrarActivos();
                    editar = false;
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Error al editar los datos por: " + ex);
                }
            }
        }

        private void btnEditar_Click(object sender, EventArgs e)
        {
            if (dataGridView1.SelectedRows.Count > 0)
            {
                editar = true;
                txtCuenta.Texts = dataGridView1.CurrentRow.Cells["CUENTA"].Value.ToString();
                txtMonto.Texts = dataGridView1.CurrentRow.Cells["MONTO"].Value.ToString();
                idAct = dataGridView1.CurrentRow.Cells["ID"].Value.ToString();
            }
            else
                MessageBox.Show("Seleccione la fila a editar");
        }
    }
}
