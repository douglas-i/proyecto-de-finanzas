﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;
using FinanceSystem.DataAcess;

namespace FinanceSystem.Domain
{
    class _Pasivo:Pasivos
    {
        public DataTable MostrarPas()
        {
            DataTable tabla = new DataTable();
            tabla = MostrarPasivo();
            return tabla;
        }

        public void InsertarPas(string cuenta, string money)
        {
            InsertarPasivo(cuenta, Convert.ToDouble(money));
        }
    }
}
