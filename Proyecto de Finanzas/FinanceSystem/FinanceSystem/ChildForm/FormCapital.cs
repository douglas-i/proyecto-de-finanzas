﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using FinanceSystem.Domain;

namespace FinanceSystem.ChildForm
{
    public partial class FormCapital : Form
    {
        _Capital capital = new _Capital();
        private string idcap = null;
        private bool editar = false;

        public FormCapital()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void FormCapital_Load(object sender, EventArgs e)
        {
            MostrarCapital();
        }

        private void MostrarCapital()//Metodo para mostrar las cuentas de capital que se encuentra en la base de datos
        {
            dataGridView1.DataSource = capital.MostrarCapital();
            dataGridView1.Columns[0].Width = 65;
            dataGridView1.Columns[1].Width = 364;
            dataGridView1.Columns[2].Width = 192;
        }

        private void btnIngresar_Click(object sender, EventArgs e)
        {
            try
            {
                capital.InsertarCap(txtCuenta.Texts, txtMonto.Texts);
                MessageBox.Show("Se Inserto Correctamente");
                MostrarCapital();
            }
            catch(Exception ex)
            {
                MessageBox.Show("Error al insertar los datos por: " + ex);
            }
        }
    }
}
